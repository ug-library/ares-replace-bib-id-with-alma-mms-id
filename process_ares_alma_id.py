import configparser
import pymssql
import os
import sys
import logging.config
from AresAlmaINIDB import AresAlmaINIDB
from datetime import datetime
import pytz

BASE_DIR = os.path.dirname(os.path.realpath(__file__))

# configure a bit of logging 
ares_alma_log_file = f"{BASE_DIR}{os.sep}logs{os.sep}ares_alma_remap_info.log"
exception_log_file = f"{BASE_DIR}{os.sep}logs{os.sep}exceptions.log"

LOGGING_CONFIG = None
LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'standard': {
            'format' : "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
            'datefmt' : "%d/%b/%Y %H:%M:%S"
        },
    },
    'handlers': {
        'null': {
            'level':'DEBUG',
            'class':'logging.NullHandler',
        },                
        'ares_alma_rotating_file': {
            'level' : 'INFO',
            'formatter' : 'standard',
            'class' : 'logging.handlers.RotatingFileHandler',
            'filename' : ares_alma_log_file,
            'maxBytes' : 10000000,
            'backupCount' : 7,
        },          
        'exception_rotating_file': {
            'level' : 'INFO',
            'formatter' : 'standard',
            'class' : 'logging.handlers.RotatingFileHandler',
            'filename' : exception_log_file,
            'maxBytes' : 4000000,
            'backupCount' : 5,
        },          
        'console':{
            'level':'INFO',
            'class':'logging.StreamHandler',
            'formatter': 'standard'
        },
    },
    'loggers': {        
        'ares_alma_logger': {
            'handlers': ['ares_alma_rotating_file'],
            'level': 'INFO',
            'propagate': True,
        },        
        'exception_logger': {
            'handlers': ['exception_rotating_file'],
            'level': 'INFO',
            'propagate': True,
        },        
    }
}

# use a customTime for logging, ensure timestamps make sense within a container
def customTime(*args):
    '''
    setup logging timestamp to be eastern time
    '''
    utc_dt = pytz.utc.localize(datetime.utcnow())
    my_tz = pytz.timezone("US/Eastern")
    converted = utc_dt.astimezone(my_tz)
    return converted.timetuple()

logging.Formatter.converter = customTime

logging.config.dictConfig(LOGGING)

# instantiate logging
# load the logger this process will use
logger = logging.getLogger('ares_alma_logger')
exception_logger = logging.getLogger('exception_logger')

# start with a new log file instead of continuous roll...more for dev / unique runs
# typically will start logging in the .1 file
handler = logging.handlers.RotatingFileHandler(ares_alma_log_file, mode='w', backupCount=7)
handler.doRollover()

handler = logging.handlers.RotatingFileHandler(exception_log_file, mode='w', backupCount=5)
handler.doRollover()

def process_file(csv_path,fname, show_list, ares_db, csv_tmp_output):
    '''
    Actually read through the mapping file and perform the bib to mms updates
    '''       
    record_count = 0
    bib_id_count = 0
    read_batch_size = 5000
    update_batch_size = 600
    try:        
        # we'll process in batch sizes, to limit db reads/writes
        batch_list = []
        file_map = {}

        # create the tmp working file
        if show_list:
            tmp_path = f"{csv_tmp_output}{fname.split('.')[0]}_ares_alma_tmp.csv"

            # create an empty temp file to use, this will replace/full overwrite existing
            with open(tmp_path, "w") as tmpfile:
                tmpfile.flush()
        
        # process the input file
        fpath = f"{csv_path}{fname}"
        with open(fpath) as f:                
            for line in f:
                rec_map = {}
                info_list = line.split(",")
                
                try:
                    mms_id = info_list[0].strip()
                except Exception as e:
                        mms_id = ""
                        exception_logger.exception(e)                    
                try:
                    bib_id = info_list[1].split("-")[0].strip()
                except Exception as e:
                        bib_id = ""
                        exception_logger.exception(e)                    
                
                rec_map["mms_id"] = mms_id
                rec_map["bib_id"] = bib_id
                
                file_map[bib_id] = mms_id                

                batch_list.append(rec_map)

                rec_count = len(batch_list)

                if ((show_list) and (rec_count == read_batch_size)):                    
                    # perform a batch read, more efficent than individual sql lookups / connections
                    tmp_rec_count, tmp_bib_id_count = process_generateList(ares_db,batch_list,file_map,tmp_path)
                    record_count = record_count + tmp_rec_count
                    bib_id_count = bib_id_count + tmp_bib_id_count                                                        

                    # reset batch logic
                    batch_list = []
                    file_map = {}
                elif ((not show_list) and (rec_count == update_batch_size)):                    
                    # update multiple records at once, more time efficent
                    tmp_bib_id_count = process_updateList(ares_db,batch_list)
                    bib_id_count = bib_id_count + tmp_bib_id_count 

                    # reset batch logic
                    batch_list = []
                    file_map = {}
        
        if show_list:
            # process the rest of batch
            tmp_rec_count, tmp_bib_id_count = process_generateList(ares_db,batch_list,file_map,tmp_path)
            record_count = record_count + tmp_rec_count
            bib_id_count = bib_id_count + tmp_bib_id_count                                                        

            # reset batch logic
            batch_list = []
            file_map = {}            
        else:
            # process the rest of batch
            tmp_bib_id_count = process_updateList(ares_db,batch_list)
            bib_id_count = bib_id_count + tmp_bib_id_count
                    
            # reset batch logic
            batch_list = []
            file_map = {}            
    except Exception as e:
        exception_logger.exception(e)
    
    return record_count, bib_id_count

def process_updateList(ares_db, batch_list):
    '''
    process the batch file for the updates. Log as a sanity check
    '''
    bib_id_count = 0
    if len(batch_list) > 0:
        ares_db.updateItemsESPNumberByESPNumber(batch_list)
        for rec_map in batch_list:
            bib_id_count = bib_id_count + 1
            bib_id = rec_map.get("bib_id")
            mms_id = rec_map.get("mms_id")
            logger.info(f"Updating bib id: {bib_id} with mms id: {mms_id}")

    return bib_id_count

def process_generateList(ares_db, batch_list, file_map, tmp_path):
    '''
    This function will process the batch list and determine the full record count / affected records
    This is done because multiple records of one bib id could be present
    '''

    # add a couple more known bibs
    #rec_map = {}
    #rec_map["mms_id"] = "9910245783505154"
    #rec_map["bib_id"] = "1024578"
    #file_map["1024578"] = "9910245783505154"
    #batch_list.append(rec_map)

    #rec_map = {}
    #rec_map["mms_id"] = "9910253403505154"
    #rec_map["bib_id"] = "1025340"
    #file_map["1025340"] = "9910253403505154"
    #batch_list.append(rec_map)
    batch_record_count = 0
    bid_id_count = 0
    if len(batch_list) > 0:
        # get all the records for that bib, could be multiple, not needed, but a nice to know
        item_list = ares_db.getItemsByESPNumber(batch_list)                                        
        batch_record_count = len(item_list)

        prev_item = ""
        item_mms_id = ""
        rec_size = 0
        
        if batch_record_count > 0:
            # append to the tmp working file
            with open(tmp_path, "a") as tmpfile:                
                for item in item_list:
                    item_bib_id = item.get("ESPNumber", "")
                    item_mms_id = file_map.get(prev_item,"")

                    if ((prev_item != "") and (prev_item != item_bib_id)):
                        # write / reset
                        bid_id_count = bid_id_count + 1
                        rec_text = "records"                                                                                                    
                        if rec_size == 1:
                            rec_text = "record"
                            logger.info(f"Located bib id: {prev_item} -> TO BE UPDATED with mms id: {item_mms_id} in {rec_size} {rec_text}")
                        else:
                            logger.info(f"Located bib id: {prev_item} -> TO BE UPDATED with mms id: {item_mms_id} in {rec_size} {rec_text}")
                        
                        tmpfile.write(f"{item_mms_id},{prev_item}-tmp\n")
                        rec_size = 0
                    
                    rec_size = rec_size + 1                        
                    prev_item = item_bib_id
                
                if rec_size > 0:
                    bid_id_count = bid_id_count + 1
                    item_mms_id = file_map.get(prev_item,"")
                    rec_text = "records"                                                                                                    
                    if rec_size == 1:
                        rec_text = "record"
                        logger.info(f"Located bib id: {prev_item} -> TO BE UPDATED with mms id: {item_mms_id} in {rec_size} {rec_text}")
                    else:
                        logger.info(f"Located bib id: {prev_item} -> TO BE UPDATED with mms id: {item_mms_id} in {rec_size} {rec_text}")   
                    tmpfile.write(f"{item_mms_id},{prev_item}-tmp\n")

    return batch_record_count, bid_id_count

if __name__ == "__main__":
    '''
    A script to process the csv files provided by Ex Libris to map old system bib id's to Alma MMS ID's.
    This mapping will replace bib id's in Ares with the Alma equivalent MMS.
    This only needs to be run once POST MIGRATE per OCUL institution running Ares
    '''    
    dt_end = None
    dt_start = None    
    eastern=pytz.timezone('America/Toronto')

    try:
        logger.info("********** RUNNING THE ARES TO ALMA BIB ID TO MMS MAP **********")                     
        dt_start = datetime.now(tz=eastern)           
        logger.info("sys args: " + str(sys.argv))

        # for each csv file in the csv_input directory, process line by line
        # and look for Ares value to replace

        # get the item list from Ares        
        record_count = 0
        bib_id_count = 0
        total_record_count = 0        
        total_bib_id_count = 0
        csv_file_count = 0

        # are we updating or skipping db updates and just generating a list
        show_list = False

        if len(sys.argv) > 1:
            func = sys.argv[1].split(' ')[0]
            if func == "list":
                show_list = True            
                logger.info("NO DATABASE UPDATES WILL BE DONE -> GENERATING RECORD LIST THAT WOULD BE IMPACTED")

        with AresAlmaINIDB() as ares_db: 
            csv_path = f"{BASE_DIR}{os.sep}csv_input{os.sep}"
            # if we're updating, use tmp files that the show process created
            csv_tmp_output = f"{BASE_DIR}{os.sep}csv_tmp_output{os.sep}" 
            if not show_list:
                csv_path = csv_tmp_output
                
            if os.path.isdir(csv_path):
                # if no courses found, must be a connection issue
                if ares_db.doCoursesExist():
                    # dir is found, get the csv files located in it
                    dir_list = os.listdir(csv_path)
                    for fname in dir_list:            
                        if ".csv" in fname:
                            csv_file_count = csv_file_count + 1  
                            logger.info(f"Processing csv file: {csv_path}{fname}")
                            record_count, bib_id_count = process_file(csv_path,fname,show_list,ares_db,csv_tmp_output)
                            if show_list:
                                logger.info(f"{record_count} Ares records WILL BE updated from file {fname}")
                                logger.info(f"{bib_id_count} unique bib id's WILL BE updated from file {fname}")
                            else:                                
                                logger.info(f"{bib_id_count} unique bib id's updated from file {fname}")

                            total_record_count = total_record_count + record_count
                            total_bib_id_count = total_bib_id_count + bib_id_count
                else:           
                    logger.info("No Ares courses found, probably a connection / .ini setup issue")
            else:           
                logger.info("CSV input path not found")
        
        if show_list:
            logger.info(f"{total_record_count} TOTAL Ares records WILL BE updated from all csv input files")
            logger.info(f"{total_bib_id_count} TOTAL unique bib id's WILL BE updated from all csv input files")
        else:            
            logger.info(f"{total_bib_id_count} TOTAL unique bib id's updated from all csv input files")

        dt_end = datetime.now(tz=eastern)

        #determine the time delta from start to finish
        delta = (dt_end - dt_start).total_seconds()                                             
    
        logger.info(f"Number of csv files processed: {csv_file_count}")
        logger.info(f"********** ARES TO ALMA BIB ID TO MMS MAP PROCESS OVER AT: {dt_end} **********")        

    except Exception as e:
        exception_logger.exception(e)


