import os
import pytest
import configparser
from AresAlmaINIDB import AresAlmaINIDB

BASE_DIR = os.path.dirname(os.path.realpath(__file__))

def test_alma_data_files():
    """
    ensure the datafiles are seen within container, checking the csv_input directory
    """
    result = False
    
    fpath = f"{BASE_DIR}{os.sep}csv_input{os.sep}"    
    try:       
        if os.path.isdir(fpath):
            # dir is found, get the csv files located in it
            dir_list = os.listdir(fpath)
            for fname in dir_list:            
                if ".csv" in fname:
                    # found one, we can stop
                    result = True
                    break                
    except Exception:
        pass    

    assert result, "No csv files located in csv_input directory"

def test_config_file_exists():
    """
    ensure the ini config file exists and is available within container, same location as this file
    """
    result = False
    
    fpath = f"{BASE_DIR}{os.sep}ares_id.ini"
    try:        
        if os.path.exists(fpath):
            result = True                
    except Exception:
        pass    

    assert result, f"config file ares_id.ini not found in current directory: {BASE_DIR}"

def test_config_file_contents():
    """
    ensure the ini config file has the appropriate keys in place same location as this file
    """
    result = True
        
    try:      
        fpath = f"{BASE_DIR}{os.sep}ares_id.ini"
        config = configparser.ConfigParser()
        config.read(fpath)

        config['db']['db_host']        
        config['db']['db_user']
        config['db']['db_passwd']
            
    except Exception:
        result = False        

    assert result, "[db] setup for db_host, db_user and db_passwd not found in ares_id.ini config file"

def test_ares_db():
    """
    test the connection to the Ares database
    """
    result = True
        
    try:      
        with AresAlmaINIDB() as ares_db:
            # if no coourses found, must be a connection issue
            if not ares_db.doCoursesExist():
                result = False                           
    except Exception as e:
        print(e)
        result = False        

    assert result, "Could not connect to Ares database, due either to config file setup, or network connection issues"
