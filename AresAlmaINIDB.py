import pymssql
import configparser
from datetime import timedelta
from datetime import date
from datetime import datetime
import os
import logging

exception_logger = logging.getLogger('exception_logger')

class AresAlmaINIDB():
    '''
    This class will handle the database interactions with the ARES database
    '''
    def __init__(self, connect_server=None):        
        '''
        If connection server is passed in, use that (aids in testing) otherwise, lets get from env or the settinsg file
        '''        
        try:
            BASE_DIR = os.path.dirname(os.path.realpath(__file__))
            config = configparser.ConfigParser()
            fpath = f"{BASE_DIR}{os.sep}ares_id.ini"
            config.read(fpath)
            
            db_user = config['db']['db_user']
            db_passwd = config['db']['db_passwd']
        
            if connect_server == None:
                connect_server = config['db']['db_host']        
            self.conn = pymssql.connect(
                server=connect_server,
                user=db_user,
                password=db_passwd,               
                database="AresData")     
            
            self.cursor = self.conn.cursor(as_dict=True)
        except Exception as e:                        
            exception_logger.exception(e)                         
    
    def __enter__(self):
        '''
        setting the class up to work with the 'with' statement to aid in closing the DB connection
        '''
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        '''
        ensure that we try to close the db connection
        '''
        try:
            self.cursor.close()
            self.conn.close()
        except:
            pass

    def doCoursesExist(self):
        '''
        used more as a connection test, just ensure a course is in the system        
        return: bool
        '''
        sql_course = 'select count(*) as test from courses'
        db_results = {}
        result = False

        try:
            self.cursor.execute(sql_course)
            db_results = self.cursor.fetchone()                    

            if db_results != None:
                result = True
        except Exception as e:                        
            exception_logger.exception(e)             

        return result            

    def getItemsByESPNumber(self,batch_list=[]):
        '''
        Get the course items by espnumber, bib id / mms id. A list of records maps passed in
        rec_map["mms_id"] = mms_id
        rec_map["bib_id"] = bib_id
        '''

        db_results_list = []        
        in_statment = ""
        try:
            # build an sql query from the list
            if len(batch_list) > 0:
                for rec_map in batch_list:
                    bib_id = rec_map.get("bib_id")
                    in_statment = f"{in_statment}'{bib_id}',"

                in_statment = in_statment[0:len(in_statment) - 1]

                sql_items = f"SELECT * FROM items WHERE ESPNumber IN ({in_statment}) ORDER BY ESPNumber"
                self.cursor.execute(sql_items)        

                for row in self.cursor:
                    db_results_list.append(row)
        except Exception as e:                        
            exception_logger.exception(e)                    

        return db_results_list            

    def updateItemsESPNumberByESPNumber(self,batch_list=[]):
        '''
        update the ESPNumber on multiple item records by ESPNumber
        '''             
        # we'll use the following column / case pattern to update
        # quick update type for updating one field and multiple records, not good for multi field

        #UPDATE theTable
        #SET theColumn = CASE
            #WHEN id = 1 then 'a' 
            #WHEN id = 2 then 'b'
            #WHEN id = 3 then 'c'
        #END
        #WHERE id in (1, 2, 3)   

        sql = "UPDATE items SET ESPNumber = CASE"
        in_statment = ""        

        try:
            # build an update query from the list based on case
            if len(batch_list) > 0:
                for rec_map in batch_list:
                    bib_id = rec_map.get("bib_id")
                    mms_id = rec_map.get("mms_id")

                    sql = f"{sql} WHEN ESPNumber = '{bib_id}' THEN '{mms_id}'"
                    in_statment = f"{in_statment} '{bib_id}',"

                in_statment = in_statment[0:len(in_statment) - 1]
                in_statment = in_statment.strip()
                sql = f"{sql} END WHERE ESPNumber IN ({in_statment})"                            
                # perform update
                self.cursor.execute(sql)
                
                # you must call commit() to persist your data if you don't set autocommit to True
                self.conn.commit()      
        except Exception as e:
            exception_logger.exception(e)                   

        return                                              

          