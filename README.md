# Replace old bib id's in Ares with Alma mms id post go live / migration

## Situation
Our institution store's the bib id within the Ares Course Reserve item record (ESPNumber), other institutions may not store the bib and have no need for this utility, or store the bib in another location, which will then require minor tweaks. With that, we're then able to look up availablility status when the student views the physical items details.

So, VERY IMPORTANT DETAIL -> ESPNUMBER in Item record is REPLACED.

When migrating to Alma, all items are assigned new mms id's, replacing the bib id's, so these bib's must be replaced within Ares, especially as our faculty
clone courses and the course item lists for other years / semesters.

To see the Alma API code that Ares call to get loan availability: https://gitlab.com/ug-library/ares-replace-bib-id-with-alma-mms-id/snippets/1919528

This utility will build a list of the number of Ares records that will be changed for each bib.
the log output is as follows:

NO DATABASE UPDATES WILL BE DONE -> GENERATING RECORD LIST THAT WOULD BE IMPACTED
[26/Sep/2019 18:03:56] INFO [ares_alma_logger:271] Processing csv file: /code/csv_input/01OCUL_GUE_001_BIB_IDs.csv
[26/Sep/2019 18:03:57] INFO [ares_alma_logger:209] Located bib id: 1012693 -> TO BE UPDATED with mms id: 9910126933505154 in 1 record
[26/Sep/2019 18:03:57] INFO [ares_alma_logger:209] Located bib id: 1012780 -> TO BE UPDATED with mms id: 9910127803505154 in 1 record
[26/Sep/2019 18:03:57] INFO [ares_alma_logger:211] Located bib id: 1020828 -> TO BE UPDATED with mms id: 9910208283505154 in 2 records

This will also generate working files for the update. So if file one from Ex Libris contains 1000000 mms - bib pairs, and only 5000 of
those appear within ares, then the working file will be the 5000 paired records. These are then used in the next step / process to 
perform the actual update. 

Another run of the utility will perform the update / replacement using the working file generated.

our institution has direct access to the Ares SQL database, so this is using SQL to update the records. Other instituions who use the 
Ares API could change the file AresAlmaINIDB.py

An API run could take a lot of time, as millions of requests would be required.

## How to run / test

### General info
Since this is a one time job you could run from anywhere, and probably don't want to even put on a server.

This repository / job can be run as a docker container, you can install docker desktop from here: https://www.docker.com/products/docker-desktop

this way, you don't need to know how to setup a python environment. Once docker is installed, ensure it's running.

This repository/project was built using Visual Studio Code and uses the remote-containers extension. Using this, you could debug the script
if you need to make changes. details on this are located with the VSC_README.md README file and more details here: https://devblogs.microsoft.com/python/remote-python-development-in-visual-studio-code/

As an aside, the remote-containers extension and docker is a nice way to do local dev work for the PrimoVE.

### Setup

1. Download or clone the repository
2. copy the sample.ini file and create ares_id.ini in the same root location (where the .py files exist)
3. in ares_id.ini place your Ares database coonection info under the [db] section, or if you're making changes and using the api, place your info here. This version is currently only using the [db] section
4. place all your csv migration files from Ex Libris in the csv_input directory. The script will process all csv files located in this dir.

### Actual commands
You could use docker compose to run this if familiar, I'll include those simplier instructions at the end, but I'll provide more detailed instruction with the following explict commands to make it a little more clear.

#### detailed commands
In the run commands, we must specify an absolute path to the logs directory and the csv_tmp_output directory, to ensure we have the log files after the container job finishes running, and we can easily view if desired.
The commands below will be: docker run -v [FULL_PATH]:/code/logs -v [FULL_PATH]:/code/csv_tmp_output --rm py-ares-alma-docker-dev-update
where you must change FULL_PATH to your project/repository logs folder. 

eg: docker run -v /Users/zeus/gitdev/ares-replace-bib-id-with-alma-mms-id/logs:/code/logs -v /Users/zeus/gitdev/ares-replace-bib-id-with-alma-mms-id/csv_tmp_output:/code/csv_tmp_output --rm py-ares-alma-docker-dev-update

1. open a terminal or command prompt and change to the repository directory (where the Dockerfile is located)
2. docker build -t py-ares-alma-docker-dev-run-tests . -f Dockerfile.tests
3. That will build the test container. We'll run this to ensure all setup critera is met (ares_id.ini exists, db connection found, csv files in place)
4. docker run -v [FULL_PATH]:/code/logs -v [FULL_PATH]:/code/csv_tmp_output --rm py-ares-alma-docker-dev-run-tests
5. That will run the tests, you'll want to see that 4 tests passed, output should be this:

ares-replace-bib-id-with-alma-mms-id (master*) » docker run -v /Users/zeus/gitdev/ares-replace-bib-id-with-alma-mms-id/logs:/code/logs --rm py-ares-alma-docker-dev-run-tests                                 ~/gitdev/ares-replace-bib-id-with-alma-mms-id
============================= test session starts ==============================
platform linux -- Python 3.7.4, pytest-5.1.3, py-1.8.0, pluggy-0.13.0
rootdir: /code
collected 4 items

test_ares_alma_id.py ....                                                [100%]

=============================== warnings summary ===============================
AresAlmaINIDB.py:1
  /code/AresAlmaINIDB.py:1: DeprecationWarning: Using or importing the ABCs from 'collections' instead of from 'collections.abc' is deprecated, and in 3.8 it will stop working
    import pymssql

-- Docs: https://docs.pytest.org/en/latest/warnings.html
======================== 4 passed, 1 warnings in 0.03s =========================

6. docker build -t py-ares-alma-docker-dev-gen-lists . -f Dockerfile.list_impacted
7. that will build the container that will generate the list, see how many records we'll be impacting. takes a couple minutes per csv (1 million)
8. docker run -v [FULL_PATH]:/code/logs -v [FULL_PATH]:/code/csv_tmp_output --rm py-ares-alma-docker-dev-gen-lists
9. Look at the files in the logs folder, start with ares_alma_remap_info.log.1, depending on the number of csv files and records, you may only have one
these are rolling log files. Look at the bottom of the file to locate the total number of files processed and records impacted, does it make sense
10. docker build -t py-ares-alma-docker-dev-update .
11. that will build the container that will perform the actual update
12. You may want to backup the log files / generated list, the update will overwrite / cause these log files to roll over
13. looks good, time to perform the update. This could take a long time, it's around 2 hours per csv file (per million), DB updates are more intensive
14. docker run -v [FULL_PATH]:/code/logs -v [FULL_PATH]:/code/csv_tmp_output --rm py-ares-alma-docker-dev-update
15. All records should be done, you can try generating the list again if you like, it should find zero records this time, because they've been remapped to the mms id

#### Docker Compose commands

1. in the docker-compose.yml and the docker-compose-update.yml files changes the paths under the 'volume' section to match your environment.
2. in the comamnd line run the following to generate the log and temp working files that have the bibs in Ares: docker-compose up --build
3. to perform the update: docker-compose -f docker-compose-update.yml up --build
4. Both will perform the tests before, so you can see if the setup is correct
